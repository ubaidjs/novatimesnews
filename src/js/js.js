let news = {}; // this object will contain array of articles
const apiKey = 'e040bab40e824735bf568c5532951cc8';

//get the value from user input
function getQuery() {
    let query = document.querySelector('.search__input').value;
    document.querySelector('.search__input').value = '';
    document.querySelector('.hd').innerHTML = '';
    renderLoader(document.querySelector('.hd'));
    getNews(query);
}

//Loader
const elementString = {
    loader: 'loader',
};
const renderLoader = parent => {
    const loaderMarkup = `
        <div class="${elementString.loader}">
        <img src="dist/icons/4V0b.gif" alt="loading...">
        </div> 
    `;
    parent.insertAdjacentHTML('afterbegin', loaderMarkup);
};
const clearLoader = () => {
    const loader = document.querySelector(`.${elementString.loader}`);
    if (loader) loader.parentElement.removeChild(loader);
};

//event listners for user input - search and enter button
document.querySelector('.submit__btn').addEventListener('click', getQuery);
document.addEventListener('keypress', function (event) {
    if (event.keyCode === 13 || event.which === 13) {
        getQuery();
        
    }
});

//Get data from APi
async function getNews(query) {
    try {
        const result = await fetch(`https://newsapi.org/v2/everything?q=${query}&language=en&sortBy=popularity&apiKey=${apiKey}`);
        const data = await result.json();
        news = data.articles;
        clearLoader();
        news.forEach(e => {
            renderResult(e);
        });
    } catch (e) {
        console.log(e);
    }
}

//display the specific news that user clicked on
document.querySelector('.hd').addEventListener('click', el => {
    let target;
    if (el.target.matches('.title')) {
        target = el.target;
    }

    let found = news.find(e => {
        return e.title === target.textContent;
    });

    renderNews(found);
});

//render headlines under recent news section
const renderResult = newsData => {
    const markup = `
            <a href="#" class="headline__link">
                <div class="headlines__item" data-idd="${newsData.idd}">
                    <p class="title">${newsData.title}</p>
                    <p class="date">${newsData.publishedAt.slice(0, 10)}</p>
                </div>
            </a>
        `;
    document.querySelector('.hd').insertAdjacentHTML('beforeend', markup);
};

//render news when user clicked on headline
const renderNews = newsObj => {
    const markup = `<h1 class="news-title">${newsObj.title}</h1>
    <p class="author"><i>by</i> ${newsObj.author}</p>
    <p class="date">${newsObj.publishedAt.slice(0, 10)}</p>
    <div class="news-img-div">
        <img src="${newsObj.urlToImage}" alt="[IMAGE]" class="news-image">
    </div>
    <p class="detail">${newsObj.content}</p>
    <a href="${newsObj.url}" class="url-link" target="_blank">READ FULL ARTICLE</a>`

    document.querySelector('.bottom').innerHTML = '';
    document.querySelector('.bottom').insertAdjacentHTML('afterbegin', markup);
}

//event listners for navigation icons
let categoryEl = document.querySelectorAll('.category__link'); //array

categoryEl.forEach(el => {
    el.addEventListener('click', () => {
        document.querySelector('.hd').innerHTML = '';
        renderLoader(document.querySelector('.hd'));
        getNews(el.id);
    })
});

// enabling night mode
document.querySelector('.nightmode').addEventListener('click', () => {
   alert('\'Night Mode\' functionality is still under process'); 
});
document.querySelector('.saveforlater').addEventListener('click', () => {
    alert('\'Save For Later\' functionality is still under process'); 
 })

